define([
	'require',
	'madjoh_modules/ajax/ajax',
],
function (require, AJAX){
	var Friendship = {
		// SEARCH & SUGGEST
			search : function(startIndex, options){
				var parameters = {prefix : options.specificity};
				if(startIndex) parameters.startIndex = startIndex;

				return AJAX.post('/friendship/search', {parameters : parameters}).then(function(result){
					return result.data.users;
				});
			},
			suggest : function(startIndex, options){
				var facebook_ids = options.specificity || [];
				var parameters = {facebook_ids : JSON.stringify(facebook_ids)};
				if(startIndex) parameters.startIndex = startIndex;

				return AJAX.post('/friendship/suggest', {parameters : parameters}).then(function(result){
					return result.data.users;
				});
			},
			getFacebook : function(startIndex, options){
				var facebook_ids = options.specificity || [];
				var parameters = {
					facebook_ids : JSON.stringify(facebook_ids)
				};
				if(options.suggestable) parameters.suggestable 	= true;
				if(startIndex) 			parameters.startIndex 	= startIndex;

				return AJAX.post('/friendship/get/facebook', {parameters : parameters}).then(function(result){
					return result.data.users;
				});
			},

		// ACTIONS
			addMultiple 	: function(friend_ids){
				if(!friend_ids || friend_ids.length === 0) return;
				if(typeof friend_ids !== 'string') friend_ids = JSON.stringify(friend_ids);
				return AJAX.post('/friendship/add/multiple', {parameters : {friend_ids : friend_ids}});
			},
			add 			: function(friend_id){return AJAX.post('/friendship/add', 		{parameters : {friend_id : friend_id}});},
			remove 			: function(friend_id){return AJAX.post('/friendship/remove', 	{parameters : {friend_id : friend_id}});},
			block 			: function(friend_id){return AJAX.post('/friendship/block', 	{parameters : {friend_id : friend_id}});},
			ignore 			: function(friend_id){return AJAX.post('/friendship/ignore', 	{parameters : {friend_id : friend_id}});},

		// GETTERS
			getList : function(route, startIndex, friend_id){
				var parameters = {};
				if(startIndex) 	parameters.startIndex 	= startIndex;
				if(friend_id) 	parameters.friend_id 	= friend_id;

				return AJAX.post(route, {parameters : parameters}).then(function(result){return result.data.users;});
			},
			getFriends 		: function(startIndex, options){return Friendship.getList('/friendship/get/friends', 		startIndex, options.specificity);},
			getAdded 		: function(startIndex, options){return Friendship.getList('/friendship/get/added', 			startIndex, options.specificity);},
			getAddedMe 		: function(startIndex, options){return Friendship.getList('/friendship/get/added_me', 		startIndex, options.specificity);},
			getAddedMeAll 	: function(startIndex, options){return Friendship.getList('/friendship/get/added_me/all', 	startIndex, options.specificity);},
			getCommon 		: function(startIndex, options){return Friendship.getList('/friendship/get/common', 		startIndex, options.specificity);},
			getProfile 		: function(friend_id){
				var parameters = {};
				if(friend_id) parameters.friend_id = friend_id;
				return AJAX.post('/user/profile', {parameters : parameters}).then(function(result){return result.data.user;});
			},
			getUsers : function(startIndex, options){
				if(!options) options = {};
				var parameters = {user_ids : JSON.stringify(options.specificity)};
				if(startIndex) 	parameters.startIndex 	= startIndex;
				return AJAX.post('/friendship/get/users', {parameters : parameters}).then(function(result){return result.data.users;});
			}
	};

	return Friendship;
});